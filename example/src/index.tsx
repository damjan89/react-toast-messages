import * as React from 'react';
import * as ReactDOM from 'react-dom';
import ReactToastMessages from 'react-toast-messages';
export interface IState {
    status:string,
    message: string,
    timeout: number,
}
export default class ReactToastMessagesExample extends React.Component<{}, IState> {
  constructor(props:any) {
      super(props);
      this.state = {
          status: 'warning', // success|danger|info|warning
          message: '',
          timeout: 2000
      }
  }
  componentDidMount(){

  }
  show(){
      let self = this;
          self.setState({
              message:'This is test toast message'
          });
          setTimeout(function () {
              self.setState({
                  message:''
              });
          }, this.state.timeout);
  }
  render() {
    return (
    <div style={{width: '100%'}}>
        <button onClick={()=>this.show()}>Show Toggle</button>
        <ReactToastMessages status={this.state.status} message={this.state.message} timeout={this.state.timeout}></ReactToastMessages>
    </div>
    );
  }
}
ReactDOM.render(<ReactToastMessagesExample/>, document.getElementById('root'));
